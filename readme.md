# Projet Couleur
Le projet couleur est un petit projet de traitement d'images.
Le but est de pouvoir filtrer l'image, extraire des formes, des statistiques (éventuellement) sur les images chargées.

## Requis
- python3x
- pip
- **tkinter** :  pip install python-tk
- **PIL** : pip install Pillow

## Lancement
Le projet couleur se lance comme tout projets python qui se respectent:

    python3 couleur.py
