#!/usr/bin/env python3
# coding: utf-8

from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from PIL import ImageTk, Image


class App(Tk):
    def __init__(self):
        super().__init__()
        """App est une classe fille de Tk. Tk permet de créer un élément la fenêtre principale d'une application graphique.
        """
        self.geometry("250x250")
        self.windows = []

        # Initialise des fenêtres séparées
        self.win_filter = self.add_window('Filters')
        self.win_couleur = self.add_window('Couleurs')
        # Initialise les scrollbars
        self.scroll_x = Scrollbar(self, orient=HORIZONTAL)
        self.scroll_y = Scrollbar(self, orient=VERTICAL)
        self.image = None
        self._image = None
        self.pil_image = None
        self.scale = 1
        # Initialise le canvas dans la fenêtre principale
        self.canvas = Canvas(self, bg="#eeeeee", width=500, height=300)
        self.canvas.pack(side=LEFT, expand=1, fill="both")

        # Initialisation : Masque les fenêtres de menu
        for win in self.windows:
            win.toggle()

        # Génération du menu
        self.menubar = Menu(self)

        # Rubriques générales
        self.menu_file = Menu(self.menubar, tearoff=False)
        self.menu_view = Menu(self.menubar, tearoff=False)

        self.menu_file.add_command(label='Open', command=self.open_file)
        self.menu_file.add_command(label='Save', state=DISABLED, command=self.save_file)
        self.menu_file.add_command(label='Close Image', state=DISABLED, command=self.close_image)
        self.menu_file.add_command(label='Quit', command=self.destroy)

        self.menu_view.add_cascade(label='Show / Hide filters window', command=self.win_filter.toggle)
        self.menu_view.add_cascade(label='Show / Hide Colors window', command=self.win_couleur.toggle)

        self.menubar.add_cascade(label="File", menu=self.menu_file, command=None)
        self.menubar.add_cascade(label="View", menu=self.menu_view)
        self.config(menu=self.menubar)

        # Les flèches du clavier permettent de se déplacer dans l'image
        self.bind("<Up>", self.scroll_up_image)
        self.bind("<Down>", self.scroll_down_image)
        self.bind("<Left>", self.scroll_left_image)
        self.bind("<Right>", self.scroll_right_image)

        # Zoom / dezoom avec ctrl+molette
        self.bind("<Control-MouseWheel>", self.zoom_wheel)
        self.bind("<Control-Button-2>", self.reset_zoom_image)

        self.bind("<Control-o>", self.open_file)

    def add_window(self, title):
        win = Window(self, title)
        self.windows.append(win)
        return win

    def open_file(self, param=None):
        """Ouvre (ou charge) l'image sélectionnée dans le fichier spécifié"""

        file = filedialog.askopenfilename(initialdir="C:/", title="Select file",
                                          filetypes=(
                                          ("Image files", "*.jpg *.png *.bmp *.gif *.jpeg "), ("all files", "*.*")))

        if file != "":
            try:
                self.pil_image = Image.open(file)
                self._image = ImageTk.PhotoImage(self.pil_image)
                self.image = self._image._PhotoImage__photo
                self.draw_image()
            except:
                messagebox.showerror(title="Mauvais format d'image",
                                     message="Le fichier que vous tentez d'ouvrir n'est pas une image")

    def save_file(self):
        file = filedialog.asksaveasfilename(initialdir="C:/", title="Select file",
                                            filetypes=(("jpeg files", "*.jpg"), ("all files", "*.*")))

    def close_image(self):
        response = messagebox.askokcancel("Fermeture de l'image",
                                          "Etes-vous sûr de vouloir continuer (aucune donnée non sauvegardée ne sera conservée) ? ")
        if response is True:
            self.canvas.pack_forget()
            self.disable_menu_close_image()
            self.disable_menu_save()
            self.geometry("250x250")
            self.disable_scroll_x()
            self.disable_scroll_y()
            self.image = None
            self._image = None
            self.pil_image = None

    def draw_image(self, resize=True, x=0, y=0):
        if resize:
            if self.winfo_screenwidth() >= self.image.width() and self.winfo_screenheight() >= self.image.height():
                self.geometry("{}x{}".format(self.image.width(), self.image.height()))
                self.disable_scroll_x()
                self.disable_scroll_y()
            else:
                self.geometry("{}x{}".format(800, 600))
                self.activate_scroll_x()
                self.activate_scroll_y()

                self.activate_scroll_x()
                self.activate_scroll_y()
        else:
            if self.winfo_screenwidth() >= self.image.width() and self.winfo_screenheight() >= self.image.height():
                self.disable_scroll_x()
                self.disable_scroll_y()
            else:
                self.activate_scroll_x()
                self.activate_scroll_y()

                self.activate_scroll_x()
                self.activate_scroll_y()
        self.canvas.pack_forget()
        self.canvas.config(width=self.image.width(), height=self.image.height())
        self.canvas.config(scrollregion=(0, 0, self.image.width(), self.image.height()))
        self.canvas.create_image(0, 0, anchor=NW, image=self.image)

        self.canvas.pack(side=LEFT, expand=1, fill="both")
        self.scroll_x.config(command=self.canvas.xview)
        self.scroll_y.config(command=self.canvas.yview)
        self.canvas.config(xscrollcommand=self.scroll_x.set, yscrollcommand=self.scroll_y.set)


        self.canvas.config(xscrollcommand=self.scroll_x.set, yscrollcommand=self.scroll_y.set)

        self.enable_menu_close_image()
        self.enable_menu_save()

    # Fonction d'activation / désactivation des menus
    def enable_menu_close_image(self):
        self.menu_file.entryconfig("Close Image", state=NORMAL)

    def disable_menu_close_image(self):
        self.menu_file.entryconfig("Close Image", state=DISABLED)

    def enable_menu_save(self):
        self.menu_file.entryconfig("Save", state=NORMAL)

    def disable_menu_save(self):
        self.menu_file.entryconfig("Save", state=DISABLED)

    # Fonctions de de masquage des scrollbars
    def disable_scroll_x(self):
        self.scroll_x.pack_forget()

    def disable_scroll_y(self):
        self.scroll_y.pack_forget()

    def activate_scroll_x(self):
        self.scroll_x.pack(side=BOTTOM, fill=X)

    def activate_scroll_y(self):
        self.scroll_y.pack(side=RIGHT, fill=Y)

    # Fonctions de zoom/dezoom
    def zoom_wheel(self, param):
        self.canvas.pack_forget()
        if self.image is not None:
            if param.delta < 0:
                self.dezoom_image(param)
            else:
                self.zoom_image(param)

    def zoom_image(self, param=None):
        self.image = self._image._PhotoImage__photo
        if self.scale+1 <= 3:
            self.scale += 1
        else:
            self.scale = 3
        self.image = self.image.zoom(self.scale)
        if param is not None:
            self.draw_image(resize=False, x=param.x, y=param.y)
        else:
            self.draw_image(resize=False, x=0, y=0)

    def dezoom_image(self, param=None):
        self.image = self._image._PhotoImage__photo
        if self.scale-1 > 0:
            self.scale -= 1

        if self.scale >= 1:
            self.image = self.image.zoom(self.scale)

        if param is not None:
            self.draw_image(resize=False, x=param.x, y=param.y)
        else:
            self.draw_image(resize=False, x=0, y=0)

    def reset_zoom_image(self, param=None):
        self.image = self._image._PhotoImage__photo
        if param is not None:
            self.draw_image(resize=False, x=param.x, y=param.y)
        else:
            self.draw_image(resize=False, x=0, y=0)

    # Fonctions de scroll au clavier
    def scroll_left_image(self, param):
        if self.image is not None:
            self.canvas.xview_scroll(-1, "units")

    def scroll_right_image(self, param):
        if self.image is not None:
            self.canvas.xview_scroll(1, "units")

    def scroll_up_image(self, param):
        if self.image is not None:
            self.canvas.yview_scroll(-1, "units")

    def scroll_down_image(self, param):
        if self.image is not None:
            self.canvas.yview_scroll(1, "units")


class Window(Frame):
    def __init__(self, parent, title=""):
        super().__init__(parent)
        self.title = title
        self.is_loaded = False
        self.parent = parent
        self.top = Toplevel(self.parent)
        if title != "":
            self.top.title(title)

        # Explicite l'action à réaliser sur les fenêtres filles de l'application quand on les ferme avec la croix
        # Cela permet de les masquer avec self.toggle. Sinon ça ferme l'instance et on ne peut plus la récupérer
        self.top.protocol("WM_DELETE_WINDOW", self.toggle)

    def toggle(self):
        if self.top.state() == 'normal':
            self.top.withdraw()
        else:
            self.top.deiconify()
